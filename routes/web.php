<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::get('dashboard', [HomeController::class, 'dashboard'])->name('dashboard');

Route::prefix('customers')->group(function () {
    Route::get('/', [CustomerController::class, 'index'])->name('customers');
    Route::get('create', [CustomerController::class, 'create'])->name('customers.create');
    Route::post('store', [CustomerController::class, 'store'])->name('customers.store');
    Route::get('edit/{customer}', [CustomerController::class, 'edit'])->name('customers.edit');
    Route::put('update/{customer}', [CustomerController::class, 'update'])->name('customers.update');
    Route::delete('delete/{customer}', [CustomerController::class, 'destroy'])->name('customers.destroy');
    Route::get('/{customer}', [CustomerController::class, 'show'])->name('customers.show');
});
