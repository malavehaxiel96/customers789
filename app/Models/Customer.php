<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'lastname',
        'mothers_lastname',
        'email',
        'phone'
    ];

    public function scopeOrderByName($query)
    {
        return $query->orderBy('name')->orderBy('lastname');
    }

    public function getLastNamesAttribute()
    {
        return "{$this->lastname} {$this->mothers_lastname}";
    }

    public function addresses()
    {
        return $this->hasMany(CustomerAddress::class, 'customer_id');
    }
}
