<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    use HasFactory;

    public $table = 'customers_addresses';

    public $fillable = [
        'street',
        'num_ext',
        'num_int',
        'colony',
        'state',
        'country',
        'customer_id'
    ];
}
