<?php

namespace App\Http\Requests\Customers;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'mothers_lastname' => 'nullable|max:255',
            'phone' => 'required|max:10',
            'address' => 'array',
            'address.*.street' => 'required',
            'address.*.num_ext' => 'required|integer',
            'address.*.num_int' => 'nullable|integer',
            'address.*.colony' => 'required',
            'address.*.state' => 'required',
            'address.*.country' => 'required'
        ];

        if ($this->routeIs('customers.update'))
        {
            $customer = $this->route()->customer;
            $rules['email'] = 'required|email|max:255|unique:customers,email,'.$customer->id;
        }
        else{
            $rules['email'] = 'required|email|max:255|unique:customers,email';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'address.*.street.required' => 'El campo Calle de las direcciones es obligatorio',
            'address.*.num_ext.required' => 'El campo Numero ext de las direcciones es obligatorio',
            'address.*.num_ext.integer' => 'El campo Numero ext debe ser un número entero',
            'address.*.num_int.integer' => 'El campo Numero int debe ser un número entero',
            'address.*.colony.required' => 'El campo Colonia de las direcciones es obligatorio',
            'address.*.state.required' => 'El campo Estado de las direcciones es obligatorio',
            'address.*.country.required' => 'El campo País de las direcciones es obligatorio'
        ];
    }
}
