<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customers\CreateCustomerRequest;
use App\Models\Customer;
use App\Processors\Customers\CustomersProccesor;
use App\Repositories\Customers\CustomersRepository;
use DB;

class CustomerController extends Controller
{
    protected $repCustomer;
    protected $pcsCustomers;

    public function __construct(CustomersRepository $repCustomer, CustomersProccesor $pcsCustomers)
    {
        $this->repCustomer = $repCustomer;
        $this->pcsCustomers = $pcsCustomers;
    }

    public function index()
    {
        $customers = $this->repCustomer->getAllbyNameOrder();

        return view('app.customers.index', compact('customers'));
    }

    public function create()
    {
        return view('app.customers.create');
    }

    public function store(CreateCustomerRequest $request)
    {
        DB::beginTransaction();

        $customer = $this->pcsCustomers->store($request);

        DB::commit();

        return redirect()->route('customers.show', $customer->id)->with(['message' => 'Cliente guardado exitosamente.']);
    }

    public function edit(Customer $customer)
    {
        return view('app.customers.edit', compact('customer'));
    }

    public function show(Customer $customer)
    {
        return view('app.customers.show', compact('customer'));
    }

    public function update(CreateCustomerRequest $request, Customer $customer)
    {
        DB::beginTransaction();

        $this->pcsCustomers->update($request, $customer);

        DB::commit();

        return redirect()->route('customers.show', $customer->id)->with(['message' => 'Información del cliente actualizada exitosamente.']);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();

        return redirect()->route('customers')->with(['message' => 'Cliente eliminado exitosamente.']);
    }
}
