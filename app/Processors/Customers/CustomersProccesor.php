<?php

namespace App\Processors\Customers;

use App\Models\Customer;
use App\Processors\Processor;
use Symfony\Component\HttpFoundation\Request;

class CustomersProccesor extends Processor {


    public function store(Request $request)
    {
        // Crea un nuevo cliente
        $customer = Customer::create($request->all());

        // Crea las direcciones
        if (! is_null($request->input('address')))
        {
            foreach ($request->input('address') as $value)
            {
                $this->createAddress($customer, $value);
            }
        }

        return $customer;
    }

    public function update(Request $request, Customer $customer)
    {
        // Actualiza la informacion general del cliente
        $customer->fill($request->all());
        $customer->save();

        // borra todos las direcciones que el usuario quito en el formalario
        $this->deleteAllExceptCurrentAddressForm($request, $customer);

        // Creacion de direcciones
        if (! is_null($request->input('address')))
        {
            foreach ($request->input('address') as $value)
            {
                // Actualiza un direccion existente
                if (! is_null($value['id']))
                {
                    $address = $customer->addresses()->whereId($value['id'])->first();
                    $address->fill($value);
                    $address->update();
                }
                else
                {
                    // Crea una nueva direccion
                    $this->createAddress($customer, $value);
                }
            }
        }

        return $customer;
    }

    protected function createAddress(Customer $customer, array $data)
    {
        return $customer->addresses()->create($data);
    }

    protected function deleteAllExceptCurrentAddressForm(Request $request, Customer $customer)
    {
        $ids = [];
        $addresses = $request->input('address') ?? array();

        foreach ($addresses as $value)
        {
            if (! is_null($value['id']))
                $ids[] = $value['id'];
        }

        $customer->addresses()->whereNotIn('id', $ids)->delete();
    }
}
