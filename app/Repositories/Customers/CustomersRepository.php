<?php

namespace App\Repositories\Customers;

use App\Models\Customer;
use App\Repositories\Repository;

class CustomersRepository extends Repository {

    public $model = Customer::class;

    public function getAllbyNameOrder()
    {
        return $this->orderByName()->get();
    }
}
