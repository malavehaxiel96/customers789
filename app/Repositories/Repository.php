<?php

namespace App\Repositories;


abstract class Repository {

    protected $model;

    public function __construct()
	{
        $this->init_model();
	}

    public function all()
	{
		return $this->model->all();
	}

    protected function init_model()
    {
        $this->model = new $this->model;
    }

    public function __call($method, $params)
	{
        $this->init_model();

		$query = $this->model->newQuery();

		return call_user_func_array([$query, $method], $params);
	}
}
