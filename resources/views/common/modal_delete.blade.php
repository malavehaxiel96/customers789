
{!! Form::open(['route' => [$route, $params], 'method' => $method ?? 'DELETE']) !!}
<!--BorderLess Modal Modal -->
<div class="modal fade text-left modal-borderless" id="{{ $id }}" tabindex="-1"
    role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $header }}</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    {{ $body ?? '¿Confirme si desea eliminar este registro?'}}
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary" data-bs-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">No</span>
                </button>
                <button type="submit" class="btn btn-primary ml-1">
                    <span class="d-none d-sm-block">Si</span>
                </button>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
