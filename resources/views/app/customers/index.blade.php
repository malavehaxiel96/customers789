@extends('layouts.app   ', [
    'title' => 'Clientes',
    'subtitle' => 'Gestión de clientes'
])

@section('content')

<section class="section">
    <div class="card">
        <div class="card-header">
            Listado de clientes
        </div>

        @if (session('message') != "")
            <div class="alert alert-light-success color-success"><i class="bi bi-star"></i> {{ session('message') }}</div>
        @endif

        <div class="card-body">
            <div class="d-flex flex-row-reverse mb-3" >
                <div class="justify-content-end">
                    <a href="{{ route('customers.create') }}" class="btn btn-primary ">Nuevo cliente</a>
                </div>
            </div>

            @if($customers->count() != 0)
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $value)
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->lastNames }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->phone }}</td>
                                <td>
                                    <a href="{{ route('customers.show', [$value->id]) }}" class="btn badge bg-info">Ver</a>
                                    <a href="{{ route('customers.edit', [$value->id]) }}" class="btn badge bg-primary">Editar</a>
                                    <button type="button" class="btn badge bg-danger" data-bs-toggle="modal" data-bs-target="#modal-delete-{{ $value->id }}">Eliminar</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @foreach ($customers as $value)
                    @include('common.modal_delete', [
                        'id' => "modal-delete-{$value->id}",
                        'header' => "{$value->name} {$value->lastNames}",
                        'route' => 'customers.destroy',
                        'params' => $value->id
                    ])
                @endforeach
            @else
                <div class="alert alert-light-info color-info"><i class="bi bi-star"></i> No hay registro de clientes.</div>
            @endif
        </div>
    </div>

</section>

@endsection

@include('partials.datatable', ['table' => 'table'])
