
@php
    $msjAddress = null;

    foreach ($errors->getMessages() as $key => $value)
    {
        if (stripos($key, 'address') !== false)
        {
            $msjAddress = $value[0];
            break;
        }
    }
@endphp

<section id="basic-horizontal-layouts">
<div class="row match-height">
    <div class="col-md-12 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Datos Generales</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">

                        <customers-form-generales
                            :errors_name_field="{{ json_encode($errors->first('name')) }}"
                            :errors_lastname_field="{{ json_encode($errors->first('lastname')) }}"
                            :errors_mothers_lastname_field="{{ json_encode($errors->first('mothers_lastname')) }}"
                            :errors_email_field="{{ json_encode($errors->first('email')) }}"
                            :errors_phone_field="{{ json_encode($errors->first('phone')) }}"
                            :old_name="{{ json_encode(old('name')) }}"
                            :old_lastname="{{ json_encode(old('lastname')) }}"
                            :old_mothers_lastname="{{ json_encode(old('mothers_lastname')) }}"
                            :old_email="{{ json_encode(old('email')) }}"
                            :old_phone="{{ json_encode(old('phone')) }}"
                            :action="{{ json_encode($action) }}"
                            :customer="{{ json_encode($customer ?? null) }}"
                        ></customers-form-generales>

                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Direcciones</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">

                        @if (! is_null($msjAddress))
                            <div class="alert alert-light-danger color-danger"><i class="bi bi-exclamation-circle"></i> {{ $msjAddress }}</div>
                        @endif

                        <customers-form-direcciones
                            :old="{{ json_encode(old('address')) }}"
                            :action="{{ json_encode($action) }}"
                            :customer_addresses="{{ json_encode($customer->addresses ?? null) }}"
                        ></customers-form-direcciones>

                    </div>
                </div>
            </div>
        </div>

    <div class="col-sm-12 d-flex justify-content-end">
        <a href="{{ route('customers') }}" class="btn btn-light-secondary me-1 mb-1">{{ $action == 'show' ? 'Ir atras' : 'Cancelar' }}</a>
        @if ($action == 'show')
            <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-primary me-1 mb-1">Editar</a>
        @endif
        @if ($action != 'show')
            <button type="submit" class="btn btn-primary me-1 mb-1">{{ $action == 'create' ? 'Guardar' : 'Actualizar' }}</button>
        @endif
    </div>
</div>

</section>
