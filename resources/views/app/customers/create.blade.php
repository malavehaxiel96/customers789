@extends('layouts.app   ', [
    'title' => 'Clientes',
    'subtitle' => 'Gestión de clientes',
    'header_text' => 'Nuevo cliente'
])

@section('content')


{!! Form::open(['route' => 'customers.store', 'method' => 'POST']) !!}

@csrf

@include('app.customers.partials.form', [
    'action' => 'create'
])

{!! Form::close() !!}


@endsection
