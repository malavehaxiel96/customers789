@extends('layouts.app   ', [
    'title' => 'Clientes',
    'subtitle' => 'Gestión de clientes',
    'header_text' => 'Información del cliente'
])

@section('content')

@if (session('message') != "")
    <div class="alert alert-light-success color-success"><i class="bi bi-star"></i> {{ session('message') }}</div>
@endif

@include('app.customers.partials.form', [
    'action' => 'show',
    'customer' => $customer
])

@endsection
