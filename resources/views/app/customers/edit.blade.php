@extends('layouts.app   ', [
    'title' => 'Clientes',
    'subtitle' => 'Gestión de clientes',
    'header_text' => 'Actualizar información del cliente'
])

@section('content')


{!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method' => 'PUT']) !!}

@csrf

@include('app.customers.partials.form', [
    'action' => 'edit',
    'customer' => $customer
])

{!! Form::close() !!}


@endsection
