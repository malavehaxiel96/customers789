@section('styles')
    <link rel="stylesheet" href="{{ asset('vendors/simple-datatables/style.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendors/simple-datatables/simple-datatables.js') }}"></script>
    <script>
        // Simple Datatable
        let table = document.querySelector("#" + "{{ $table }}");
        let dataTable = new simpleDatatables.DataTable(table);
    </script>
@endsection
