<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('street');
            $table->integer('num_ext');
            $table->integer('num_int')->nullable();
            $table->string('colony');
            $table->string('state');
            $table->string('country');
            $table->unsignedBigInteger('customer_id');

            $table->timestamps();

            $table->foreign("customer_id")
                ->references("id")
                ->on("customers")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_addresses');
    }
}
